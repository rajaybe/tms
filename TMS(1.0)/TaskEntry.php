<?php
include 'includes/sessionstart.php';
include 'includes/UserDetails.php';
include 'includes/insertask.php';
include 'includes/OptionValue.php';
include 'includes/DisplayTask.php';




?>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>TaskEntry</title>
    <script src='https://kit.fontawesome.com/a076d05399.js'></script>
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@200;300;400;600&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Material+Icons|Material+Icons+Outlined|Material+Icons+Two+Tone|Material+Icons+Round|Material+Icons+Sharp" rel="stylesheet">
    <link href="https://code.jquery.com/ui/1.10.2/themes/smoothness/jquery-ui.css" rel="stylesheet"/>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/js/bootstrap-datepicker.js"></script>
    <link rel='stylesheet' href='https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css'>
    <link rel="stylesheet" href="css/UserAccountNavstyle.css"/>
    <link href="css/styleEntry.css" rel="stylesheet" />
    <link rel="stylesheet" href="css/navStyleEntry1.css">
    <link href="css/bootstrap.min.css" rel="stylesheet"/>
       <script src="js/jquery.min.js"></script>        
<script src="js/date.js"></script>





</head>

<body>

<input type="checkbox" id="menu-toggle" />
  <label for="menu-toggle" class="menu-icon"><i class="fa fa-bars"></i></label>
  <div class="content-container">
    <div class="site-title">
      <h1>Task Entry</h1>
      <div class="action">
        <div class="profile" onclick="menuToggle();">
            <img src="img/user.png" alt="">
        </div>

        <div class="menu">
        
            <h3>
                @<?php echo $user; ?>
                <div>
              <?php echo $usertype; ?>
                </div>
                
                
            </h3>
            <ul>
                <li>
                    <span class="material-icons icons-size">person</span>
                    <a href="UserAccount.php">My Profile</a>
                </li>
                <!-- <li>
                    <span class="material-icons icons-size">mode</span>
                    <a href="#">Edit Account</a>
                </li> -->
                <li>
                <!-- <i class="fas fa-sign-out-alt"></i> -->
                    <span class="fas fa-sign-out-alt"></span>
                    <a href="includes/logout.php">Logout</a>
                </li>
                <!-- <li>
                    <span class="material-icons icons-size">monetization_on</span>
                    <a href="#">Budget</a>
                </li>
                <li>
                    <span class="material-icons icons-size">account_balance</span>
                    <a href="#">Finance</a>
                </li>
                <li>
                    <span class="material-icons icons-size">account_balance_wallet</span>
                    <a href="#">Wallet</a>
                </li> -->
            </ul>
        </div>
    </div>
    </div>
    <div class="ContentTask">
    <form method="POST" name="POST" id="myform">
    <input class="hide" name="UserId" value="<?php echo $user; ?>"/>
        <div class=" container">
            <div class="wrapper">
           
                <button type="button" class="btn btn-info" id="Schedule">Schedule Date</button>
                <!-- <input type="date" id="Cal" name="date"> -->
                <!-- <div class="Parentflex"> -->
                <div  class="input-group date" id="vcal" data-date-format="dd-mm-yyyy">
                <input id="datepicker" class="aj" name="date" placeholder="Date">
                <span class="input-group-addon" id="CalenderIcon"><i id="calicn" class="fas fa-calendar-week"></i></span>
                </div>
                
            </div>
            <div id="useris" <?php if ($usertype=='User'){ echo "class='hide'" ;}?>>
                <select name="SelectUserId" id="SelectUserid" >
                <option hidden>UserId</option>

<?php while ($rows = mysqli_fetch_array($resuserid)) :; ?>
<option value="<?php echo $rows[0]; ?>"><?php echo $rows[0]; ?></option>

<?php endwhile; ?>
</select>
</div>
                </div>
<!-- 
                <hr class="solid"> -->

           
        
   <table class=" table" id="Table">
        <col style="width:1% !important">
        <col style="width:2.5% !important">
	<col style="width: 1.5% !important;">
	<col style="width:2.5% !important">
    <col style="width:80% !important">
    <col style="width:2.5% !important">
    <col style="width:2.5% !important">
    <col style="width: 2.5% !important;">
    <col style="width:2.5% !important">
    <col style="width:auto">
            <thead id="hd">
                <th id="a">Sno</th>
                <th id="b">Priority</th>
                <th id="c">Categories</th>
                <th id="d">ConnetTo</th>
                <th id="e">Task</th>
                <th id="f">Option1</th>
                <th id="g">Option2</th>
                <th id="h">Option3</th>
                <th id="i">Status</th>
                <th id="j">Del</th>

            </thead>
            <tbody class="TableBod" id="tablebody">
                <tr class="all work" id="RowNo1">
                <td>
                <span class="Priority_No"></span>
                </td>
                    <td class="Priority" data-label="Priority">
                    <div class="drop">
                                <select class="selectpicker options wid" id="Pri1" name="Pri0" data-style="btn-success">
                                
                                    <option value="L">Low</option>
                                    <option value="M">Medium</option>
                                    <option value="H">High</option>

                                </select>
                            </div>
                        <input type="hidden" id="Uni_Id1" name="Uni0" value="<?php echo ($data_collector3+1); ?>" />

                    </td>
                    <td class="Categories" data-label="Categories">
                        <div class="dropdown">
                            <select class="selectpicker widt" name="Categories0" data-style="btn-success" id="dropdownMenuButton0">
                                <option hidden>Categories</option>
                                <?php while ($r1 = mysqli_fetch_array($resu1)) :; ?>
                                    <option value="<?php echo $r1[0]; ?>"><?php echo $r1[0]; ?></option>
                                <?php endwhile; ?>
                                <!-- <option value="all">All</option>
                                <option value="work">Work</option>
                                <option value="personal">Personal</option>
                                <option value="prospect">Prospect</option> -->

                            </select>
                        </div>

                    </td>

                    <td class="ConnetTo" data-label="Connet To">

                        <div class="dropdown">
                            <select class="selectpicker width" name="ConnetTo0" data-style="btn-success" id="Client0">
                                <option hidden>Client</option>
                                <?php while ($r2 = mysqli_fetch_array($res2)) :; ?>
                                    <option value="<?php echo $r2[0]; ?>"><?php echo $r2[0]; ?></option>
                                <?php endwhile; ?>
                                <!-- <option value="Jai">Jai</option>
                                <option value="Kamal">Kamal</option>
                                <option value="Ajay">Ajay</option>
                                <option value="Gopi">Gopi</option> -->
                            </select>
                        </div>


                    </td>
                    <td class="Task" data-label="Task">
                        <div class="form-group">

                            <textarea class="form-control" rows="1" name="note0" id="comment0"></textarea>
                        </div>
                    </td>

                    <td class="Option1" data-label="Option 1">
                        <div class="OptionDetails">
                            <div class="dropdown">
                                <select class="selectpicker options" id="Option10" name="Option10" data-style="btn-success">
                                <option hidden>Op1</option>
                                    <option value="Meet">Meet</option>
                                    <option value="Call">Call</option>
                                    <option value="Text">Text</option>

                                </select>
                            </div>
                            <div class="form-check" options>
                                <label for="Op10">

                                    <input type="hidden"  name="Op10" value="False">
                                    <input class="form-check-input" name="Op10" type="checkbox" value="True" id="Op10">
                                </label>

                            </div>
                        </div>
                    </td>
                    <td class="Option2" data-label="Option 2">
                        <div class="OptionDetails">
                            <div class="dropdown">
                                <select class="selectpicker options" id="Option20" name="Option20" data-style="btn-success">
                                <option hidden>Op2</option>
                                    <option value="Call">Call</option>
                                    <option value="Meet">Meet</option>
                                    <option value="Text">Text</option>
                                </select>
                            </div>
                            <div class="form-check">
                                <label for="Op20">
                                    <input type="hidden" name="Op20" value="False">
                                    <input class="form-check-input" type="checkbox" name="Op20" value="True" id="Op20">
                                </label>

                            </div>

                        </div>

                    </td>
                    <td class="Option3" data-label="Option 3">
                        <div class="OptionDetails">
                            <div class="dropdown4">
                                <select class="selectpicker options" id="Option30" name="Option30" data-style="btn-success">
                                <option hidden>Op3</option>
                                    <option value="Text">Text</option>
                                    <option value="Meet">Meet</option>
                                    <option value="Call">Call</option>

                                </select>
                            </div>
                            <div class="form-check">
                                <label for="Op30">
                                    <input type="hidden" name="Op30" value="False">

                                    <input class="form-check-input" type="checkbox" name="Op30" value="True" id="Op30" >
                                </label>

                            </div>
                        </div>
                    </td>

                    <td class="Status" data-label="Status">
                        <select class="selectpicker important ar" name="Status0" data-style="btn-info" id="arrow0" onchange="this.className=this.options[this.selectedIndex].className">
                            <option class="important ar">Not Started</option>
                            <option class="InProgress ar">In Progress</option>
                            <option class="InReview ar">In Review</option>
                            <option class="Completed ar">Completed</option>
                            <option class="Cancelled ar">Cancelled</option>
                        </select>

                      
                    </td>

                   
                    <td class="Deletebtn" data-label="Action">
                        <div class="ButtonWrapper1">
                        <a href='includes/deletetask.php?id=1' class="btn btn-outline-secondary AddRemove Trash" id="DeleteBtn0" onclick="return confirm('Due You Want To Delete Task?')"><i class="fas fa-trash-alt"></i></a>
                            <!-- <button type="submit" class="btn btn-outline-secondary AddRemove" name="DeleteSubmit" onclick="deleteR(this)"><i class="fas fa-trash-alt"></i></button> -->
                        </div>
                        <div class="hide">
                            <input name="rows[]" value="0" ></div>
                    </td>
                </tr>




            </tbody>
        </table>



        <div class="hide" id="Records">
        <span>No Records Found</span>
        </div>
        <hr>
        <div class="error-notice hide" id="msg">
            <div class="oaerror danger">
                <strong>Error</strong>-Please Enter The Value& Please try again.
            </div>
        </div>
        
        <div class="SaveBtn">
            <input name="Submit1" type="submit" class="btn btn-secondary" value="Save" onclick="return save_data()" />
            <div class="ButtonWrapper">
                <button type="button" name="add" class="btn btn-outline-secondary" id="event" class="AddRemove">Add</button>
            </div>
            <!-- <div class="ButtonWrapper">
                <a href="logout.php"><input type="button"   class="btn btn-outline-secondary" id="vibtn" class="AddRemove" value="Logout"/></a>
            </div> -->
        </div>
    </form>
    

    </div>
  </div>
  <div class="slideout-sidebar">
  <ul>
  <!-- <li ><a href="">Home</a></li> -->
 
  
      <li <?php if ($usertype=='User'){ echo "class='hide'" ;}?> ><a href="Definition.php" style="color:unset;">Definition</a></li>
      <li <?php if ($usertype=='Admin'){ echo "class='hide'" ;}?>><a href="Dashboard.php" style="color:unset;">Dashboard</a></li>
      <li><a href="TaskEntry.php" style="color: #49a0e3;font-size: 22px;">Entry</a></li>
      <li><a href="view1.php" style="color:unset;">View</a></li>
      
      <!-- <li><a href="UserAccount.php">UserAccount</a></li> -->
      <!-- <li><a href="logout.php">Logout</a></li> -->
      
    </ul>
  </div>
  <script src="js/MenuToggle.js"></script>
      
    <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
    <script src="https://code.jquery.com/ui/1.12.0/jquery-ui.min.js"></script>

    
    <script src="js/test.js"></script>
    <script src="js/Deletescript.js"></script>


    <script>
        function add_text_input() {

            var table = document.getElementById("Table");
            var TaskId= <?php echo json_encode($data_collector3); ?>;
            // var x = TaskId;
            // var z = TaskId - 1;
            // var y = TaskId+1;
            var x = table.rows.length;
            var z = x - 1;
            var y = x;
         var value = <?php echo json_encode($data_collector); ?>;
            var value1 = <?php echo json_encode($data_collector1); ?>;
            
           
            var row=table.insertRow(-1);
             row.innerHTML =
                '<tr class="all work" id="aj">' +
                '<td><span class="Priority_No"></span></td>'+

                '<td class="Priority" data-label="Priority"><div class="drop"><select class="selectpicker options wid" id="Pri' + x + '" name="Pri' + (x - 1) + '" data-style="btn-success"><option value="L">Low</option><option value="M">Medium</option><option value="H">High</option></select></div><input type="hidden" id="Uni_Id' + x + '" name="Uni' + (x - 1) + '" value=""/></td>' +
                "<td class='Categories' data-label='Categories'><div class='dropdown'><select class='selectpicker widt' name='Categories" + (x - 1) + "' data-style='btn-success' id='dropdownMenuButton" + (x - 1) + "'><option hidden>Categories</option></select></div></td>" +
                '<td class="ConnetTo" data-label="Connet To"><div class="dropdown"><select class="selectpicker width" name="ConnetTo' + (x - 1) + '" data-style="btn-success" id="Client' + (x - 1) + '"><option hidden>Client</option></select></div></td>' +
                '<td class="Task" data-label="Task"><div class="form-group"><textarea class="form-control" rows="1" name="note' + (x - 1) + '" id="comment' + (x - 1) + '"></textarea></div></td>' +
                '<td class="Option1" data-label="Option 1"><div class="OptionDetails"><div class="dropdown"><select class="selectpicker options"  name="Option1' + (x - 1) + '" data-style="btn-success" id="Option1' + (x - 1) + '"><option hidden>Op1</option><option value="Meet">Meet</option><option value="Call">Call</option><option value="Text">Text</option></select></div><div class="form-check" options><label for="Op1' + (x - 1) + '"><input type="hidden" name="Op1' + (x - 1) + '" value="False" ><input class="form-check-input" name="Op1' + (x - 1) + '" type="checkbox" value="True" id="Op1' + (x - 1) + '"></label></div></div></td>' +
                '<td class="Option2" data-label="Option 2"><div class="OptionDetails"><div class="dropdown"><select class="selectpicker options" name="Option2' + (x - 1) + '" data-style="btn-success" id="Option2' + (x - 1) + '"><option hidden>Op2</option><option value="Call">Call</option><option value="Meet">Meet</option><option value="Text">Text</option></select></div><div class="form-check"><label for="Op2' + (x - 1) + '"><input type="hidden" name="Op2' + (x - 1) + '" value="False" ><input class="form-check-input" type="checkbox" name="Op2' + (x - 1) + '" value="True" id="Op2' + (x - 1) + '"></label></div></div></td>' +
                '<td class="Option3" data-label="Option 3"><div class="OptionDetails"><div class="dropdown4"><select class="selectpicker options" name="Option3' + (x - 1) + '" data-style="btn-success" id="Option3' + (x - 1) + '"><option hidden>Op3</option><option value="Text">Text</option><option value="Meet">Meet</option><option value="Call">Call</option></select></div><div class="form-check"><label for="Op3' + (x - 1) + '"><input type="hidden" name="Op3' + (x - 1) + '" value="False"><input class="form-check-input" type="checkbox" name="Op3' + (x - 1) + '" value="True"id="Op3' + (x - 1) + '"></label></div></div> </td>' +
                '<td class="Status" data-label="Status"><select class="selectpicker important ar" name="Status' + (x - 1) + '" data-style="btn-info" id="arrow' + (x - 1) + '" onchange="this.className=this.options[this.selectedIndex].className"><option class="important ar">Not Started</option><option class="InProgress ar">In Progress</option><option class="InReview ar">In Review</option><option class="Completed ar">Completed</option><option class="Cancelled ar">Cancelled</option></select></div></td>' +
                '<td class="Deletebtn" data-label="Action"><div class="ButtonWrapper1"><a href="includes/deletetask.php?id=' + y + '" id="DeleteBtn' + (x - 1) + '" class="btn btn-outline-secondary AddRemove Trash" onClick=\'javascript: return confirm("Do You Want To Delete Task?");\'><i class="fas fa-trash-alt"></i></a></div><div class="hide"><input name="rows[]" value=' + x +'></div></td></tr>';
            row.className="all work";
            row.id="RowNo"+x;

                var selectCategories = document.getElementById("dropdownMenuButton" + (x - 1));

            var selectClient = document.getElementById("Client" + (x - 1));
            for (const val of value) {
                var option = document.createElement("option");
                option.text = val;

                selectCategories.appendChild(option);

            }
            // console.log(value1);
            for (const val of value1) {
                var option1 = document.createElement("option");
                option1.text = val;

                selectClient.appendChild(option1);

            }
            var table = document.getElementById("Table");
        var y = table.rows.length;
        var xy=document.getElementById("Uni_Id1").value;
        console.log("xy",xy);

    
        y=y-1;
        for(this.i=0;this.i<y;this.i++){
            console.log("Option1"+this.i);
            var elem= document.getElementById("Option1"+this.i);
           elem.addEventListener("click", func, false);
           function func(e){
               var a=e.target.id;
               console.log(a);
               No=a.slice(7);
               console.log(No);
           }
               document.getElementById("Option1"+this.i).onchange=function(e){
var el=this;
if(el.value==="Meet"){
    op="#Option2"+No;
    console.log(op);
    document.querySelectorAll(op+" option").forEach(option => option.remove())
    var values = ["Op2", "Call", "Text"];
    op1="Option2"+No;
    var select = document.getElementById(op1);
    for (const val of values) {
      var option = document.createElement("option");
      if (val == "Op2") {
      option.setAttribute("hidden", true);
      option.text = val;
    }
    option.text = val;
      option.value = val;
    

      select.appendChild(option);
    }
}
        else if (el.value === "Call") {
            op="#Option2"+No;
    console.log(op);
    document.querySelectorAll(op+" option").forEach(option => option.remove())
    var values = ["Op2", "Meet", "Text"];
    op1="Option2"+No;
    console.log(op1);
    var select = document.getElementById(op1);
    for (const val of values) {
      var option = document.createElement("option");
      if (val == "Op2") {
      option.setAttribute("hidden", true);
      option.text = val;
    }
    option.text = val;  
    option.value = val;
    

      select.appendChild(option);
    }    



}else if (el.value === "Text") {

    op="#Option2"+No;
    console.log(op);
    document.querySelectorAll(op+" option").forEach(option => option.remove())
    var values = ["Op2", "Meet", "Call"];
    op1="Option2"+No;
    var select = document.getElementById(op1);
    console.log(select);
    for (const val of values) {
      var option = document.createElement("option");
      if (val == "Op2") {
      option.setAttribute("hidden", true);
      option.text = val;
    }
     option.text = val;
      option.value = val;
    

      select.appendChild(option);
    }    

}
}
var elem1= document.getElementById("Option2"+this.i);
           elem1.addEventListener("click", func1, false);
           function func1(e){
               var b=e.target.id;
               console.log(b);
               No1=b.slice(7);
               console.log(No1);
           }


document.getElementById("Option2"+this.i).onchange=function(e){
var e1=this;
var Op33="#Option3"+No1;
var op3="Option3"+No1;
console.log(op3);
var option1select="Option1"+No1;
var source=document.getElementById(option1select).value;
if(source === "Meet" && e1.value=== "Call"){
    document.querySelectorAll(Op33+" option").forEach(option => option.remove())
    var values = ["Text"];
   var select = document.getElementById(op3);
    console.log(select);
    for (const val of values) {
      var option = document.createElement("option");
    
     option.text = val;
      option.value = val;
    

      select.appendChild(option);
    }    
}
else if (source === "Meet" && e1.value === "Text") {
    document.querySelectorAll(Op33+" option").forEach(option => option.remove())
    var values = ["Call"];
   var select = document.getElementById(op3);
    console.log(select);
    for (const val of values) {
      var option = document.createElement("option");
    
     option.text = val;
      option.value = val;
    

      select.appendChild(option);
    }    


}
else if (source === "Text" && e1.value === "Meet") {
    document.querySelectorAll(Op33+" option").forEach(option => option.remove())
    var values = ["Call"];
   var select = document.getElementById(op3);
    console.log(select);
    for (const val of values) {
      var option = document.createElement("option");
    
     option.text = val;
      option.value = val;
    

      select.appendChild(option);
    }    

}
else if (source === "Text" && e1.value === "Call") {
    document.querySelectorAll(Op33+" option").forEach(option => option.remove())
    var values = ["Meet"];
   var select = document.getElementById(op3);
    console.log(select);
    for (const val of values) {
      var option = document.createElement("option");
    
     option.text = val;
      option.value = val;
    

      select.appendChild(option);
    }    

}
else if (source === "Call" && e1.value === "Meet") {
    document.querySelectorAll(Op33+" option").forEach(option => option.remove())
    var values = ["Text"];
   var select = document.getElementById(op3);
    console.log(select);
    for (const val of values) {
      var option = document.createElement("option");
    
     option.text = val;
      option.value = val;
    

      select.appendChild(option);
    }    

}
else if (source === "Call" && e1.value === "Text") {
    document.querySelectorAll(Op33+" option").forEach(option => option.remove())
    var values = ["Meet"];
   var select = document.getElementById(op3);
    console.log(select);
    for (const val of values) {
      var option = document.createElement("option");
    
     option.text = val;
      option.value = val;
    

      select.appendChild(option);
    }    

}

}

}
}
        function save_data() {
            var table = document.getElementById("tablebody");
            var tableRows = table.rows.length;
            for(g = 0; g <tableRows; g++) {
                // console.log(g);
             
            
var cat=document.getElementById("dropdownMenuButton" + (g)).value;
var client=document.getElementById("Client" + (g)).value;
var note=document.getElementById("comment" + (g)).value;
var op1=document.getElementById("Option1" + (g)).value;
var op2=document.getElementById("Option2" + (g)).value;
                
               console.log(cat);
               console.log(client);
               console.log(op1);
                
            
            // console.log(tableRows);  
            
  if ((cat=="Categories")||(client=="Client")||(note=="")||(op1=="Op1")||(op2=="Op2")) {
      if (document.getElementById("msg").classList.contains("hide")) {
          document.getElementById("msg").classList.remove("hide");
          
      }
      return false;
  } 
            }
    }
        

        loadData = function() {
            // var data= [];
            var DataTask= <?php echo json_encode($Task); ?>;
            
            // if (window.localStorage.getItem("Table1") == undefined){
            //     window.localStorage.setItem("Table1", JSON.stringify(data));
            // }
        //    else{
            // let data = JSON.parse(window.localStorage.getItem("Table1"));
            for (g = 0; g < DataTask.length; g++) {
                // console.log(g);
                if (g>0) {
                    add_text_input();
                }
              var RowAttr=document.getElementById("RowNo"+(g+1));
              var DateSpace=(DataTask[g].Date).replace(/\s+/g, "");
              console.log(DateSpace);   
              RowAttr.setAttribute("date",DateSpace);
                document.getElementById("Uni_Id" + (g+1)).value =
                    DataTask[g].Id;
                    document.getElementById("Pri"+(g+1)).value=DataTask[g].Priority;
document.getElementById("dropdownMenuButton" + (g)).value =
                    DataTask[g].Categories;
                    var b= DataTask[g].Id;
                 
                    console.log(document.getElementById("DeleteBtn" + (g)));
                    document.getElementById("DeleteBtn" + (g)).href ="includes/deletetask.php?id="+b;
                document.getElementById("Client" + (g)).value = DataTask[g].ConnectTo;
                document.getElementById("comment" + (g)).value = DataTask[g].Note;
                document.getElementById("Option1" + (g)).value = DataTask[g].Option1;
                
                document.getElementById("Op1" + (g)).checked= JSON.parse(DataTask[g].Op1Status.toLowerCase());
                document.getElementById("Option2" + (g)).value = DataTask[g].Option2;
                document.getElementById("Op2" + (g)).checked = JSON.parse(DataTask[g].Op2Status.toLowerCase());
                document.getElementById("Option3" + (g)).value = DataTask[g].Option3;
                document.getElementById("Op3" + (g)).checked = JSON.parse(DataTask[g].Op3Status.toLowerCase());
                var stat=document.getElementById("arrow" + (g));
                // console.log(stat);
                var str=DataTask[g].Status;
                // console.log(str.replace(/\s+/g, ''));
                stat.classList.remove("important");
                stat.classList.add(str.replace(/\s+/g, ''));
                stat.value = DataTask[g].Status;
            
            }
        //    }
        };

        loadData();
     
    
    </script>
           <script>
           var usertype=<?php echo json_encode($usertype); ?>;
        $(document).ready(function() {
            
            console.log(usertype);
           
            

function addRemoveClass(theRows) {

    theRows.removeClass("odd even");
    theRows.filter(":odd").addClass("odd");
    theRows.filter(":even").addClass("even");
}

var rows = $("table#Table tbody tr");
const date = new Date();
var formattedDate = date.toLocaleDateString('en-GB', {
  day: 'numeric', month: 'long', year: 'numeric'
});

$("#datepicker").val(formattedDate);

formattedDate=formattedDate.replace(/\s+/g, "");
var DateValue=($("#datepicker").val()).replace(/\s+/g, "");
if(usertype!="Admin"){


if (DateValue==formattedDate){
            

            rows.filter("[date=" + formattedDate + "]").show();
            rows.not("[date=" + formattedDate + "]").hide();
            var visibleRows = rows.filter("[date=" + formattedDate + "]");
            addRemoveClass(visibleRows);
            records();
        } else {
    
            rows.show();
            addRemoveClass(rows);
    
        }


// console.log(rows);


addRemoveClass(rows);


$("#datepicker").on("change", function() {

    var selected = this.value;
    selected=selected.replace(/\s+/g, "");

    console.log(selected);
//     const date = new Date();
// // const formattedDate = date.toLocaleDateString('en-GB', {
// //   day: 'numeric', month: 'short', year: 'numeric'
// // }).replace(/ /g, '-');
// var formattedDate = date.toLocaleDateString('en-GB', {
//   day: 'numeric', month: 'long', year: 'numeric'
// });
// console.log(formattedDate);

// formattedDate=formattedDate.replace(/\s+/g, "");

    if (selected != "All") {
            

        rows.filter("[date=" + selected + "]").show();
        rows.not("[date=" + selected + "]").hide();
        var visibleRows = rows.filter("[date=" + selected + "]");
        addRemoveClass(visibleRows);
        records();
    } else {

        rows.show();
        addRemoveClass(rows);

    }

});
}

function records(){
var trs = document.querySelectorAll("#tablebody tr");
            console.log(trs);
            var count=0;
            for (var x = 0; x < trs.length; x++){
                if(trs[x].style.display =='none'){
                    console.log("a");
                    count++;
                    console.log(count);
                }
    
            }
            if(count==trs.length){
                document.getElementById("Records").classList.remove("hide");
            }
            else if(!document.getElementById("Records").classList.contains("hide")){
                document.getElementById("Records").classList.add("hide");
            }
        }

    
});
    </script>

<!--  -->
<script>
var table = document.getElementById("Table");
        var y = table.rows.length;
        y=y-1;
        for(this.i=0;this.i<y;this.i++){
            console.log("Option1"+this.i);
            var elem= document.getElementById("Option1"+this.i);
           elem.addEventListener("click", func, false);
           function func(e){
               var a=e.target.id;
               console.log(a);
               No=a.slice(7);
               console.log(No);
           }
               document.getElementById("Option1"+this.i).onchange=function(e){
var el=this;
if(el.value==="Meet"){
    op="#Option2"+No;
    console.log(op);
    document.querySelectorAll(op+" option").forEach(option => option.remove())
    var values = ["Op2", "Call", "Text"];
    op1="Option2"+No;
    var select = document.getElementById(op1);
    for (const val of values) {
      var option = document.createElement("option");
      if (val == "Op2") {
      option.setAttribute("hidden", true);
      option.text = val;
    }
    option.text = val;
      option.value = val;
    

      select.appendChild(option);
    }
}
        else if (el.value === "Call") {
            op="#Option2"+No;
    console.log(op);
    document.querySelectorAll(op+" option").forEach(option => option.remove())
    var values = ["Op2", "Meet", "Text"];
    op1="Option2"+No;
    console.log(op1);
    var select = document.getElementById(op1);
    for (const val of values) {
      var option = document.createElement("option");
      if (val == "Op2") {
      option.setAttribute("hidden", true);
      option.text = val;
    }
    option.text = val;  
    option.value = val;
    

      select.appendChild(option);
    }    



}else if (el.value === "Text") {

    op="#Option2"+No;
    console.log(op);
    document.querySelectorAll(op+" option").forEach(option => option.remove())
    var values = ["Op2", "Meet", "Call"];
    op1="Option2"+No;
    var select = document.getElementById(op1);
    console.log(select);
    for (const val of values) {
      var option = document.createElement("option");
      if (val == "Op2") {
      option.setAttribute("hidden", true);
      option.text = val;
    }
     option.text = val;
      option.value = val;
    

      select.appendChild(option);
    }    

}
}
var elem1= document.getElementById("Option2"+this.i);
           elem1.addEventListener("click", func1, false);
           function func1(e){
               var b=e.target.id;
               console.log(b);
               No1=b.slice(7);
               console.log(No1);
           }


document.getElementById("Option2"+this.i).onchange=function(e){
var e1=this;
var Op33="#Option3"+No1;
var op3="Option3"+No1;
console.log(op3);
var option1select="Option1"+No1;
var source=document.getElementById(option1select).value;
if(source === "Meet" && e1.value=== "Call"){
    document.querySelectorAll(Op33+" option").forEach(option => option.remove())
    var values = ["Text"];
   var select = document.getElementById(op3);
    console.log(select);
    for (const val of values) {
      var option = document.createElement("option");
    
     option.text = val;
      option.value = val;
    

      select.appendChild(option);
    }    
}
else if (source === "Meet" && e1.value === "Text") {
    document.querySelectorAll(Op33+" option").forEach(option => option.remove())
    var values = ["Call"];
   var select = document.getElementById(op3);
    console.log(select);
    for (const val of values) {
      var option = document.createElement("option");
    
     option.text = val;
      option.value = val;
    

      select.appendChild(option);
    }    


}
else if (source === "Text" && e1.value === "Meet") {
    document.querySelectorAll(Op33+" option").forEach(option => option.remove())
    var values = ["Call"];
   var select = document.getElementById(op3);
    console.log(select);
    for (const val of values) {
      var option = document.createElement("option");
    
     option.text = val;
      option.value = val;
    

      select.appendChild(option);
    }    

}
else if (source === "Text" && e1.value === "Call") {
    document.querySelectorAll(Op33+" option").forEach(option => option.remove())
    var values = ["Meet"];
   var select = document.getElementById(op3);
    console.log(select);
    for (const val of values) {
      var option = document.createElement("option");
    
     option.text = val;
      option.value = val;
    

      select.appendChild(option);
    }    

}
else if (source === "Call" && e1.value === "Meet") {
    document.querySelectorAll(Op33+" option").forEach(option => option.remove())
    var values = ["Text"];
   var select = document.getElementById(op3);
    console.log(select);
    for (const val of values) {
      var option = document.createElement("option");
    
     option.text = val;
      option.value = val;
    

      select.appendChild(option);
    }    

}
else if (source === "Call" && e1.value === "Text") {
    document.querySelectorAll(Op33+" option").forEach(option => option.remove())
    var values = ["Meet"];
   var select = document.getElementById(op3);
    console.log(select);
    for (const val of values) {
      var option = document.createElement("option");
    
     option.text = val;
      option.value = val;
    

      select.appendChild(option);
    }    

}

}

}
o = document.getElementById("event");
if(usertype=="Admin"){
numOfIncr = document.getElementById("Uni_Id1").value;
}
else{
    numOfIncr=<?php echo json_encode($data_collector3); ?>;
}
function increment(numberOfIncrementCalls){
    numberOfIncrementCalls++;
    return numberOfIncrementCalls;
    }
o.addEventListener("click",function(){
    if(!document.getElementById("Records").classList.contains("hide")){
                document.getElementById("Records").classList.add("hide");
            }
    var table = document.getElementById("Table");
    numOfIncr = increment(numOfIncr);
    var TaskId = numOfIncr;
var x = table.rows.length;
    var z = x - 1;
    var y = x;
 var value = <?php echo json_encode($data_collector); ?>;
    var value1 = <?php echo json_encode($data_collector1); ?>;
    var row1=table.insertRow(-1);
   
    
     row1.innerHTML ='<tr class="aj" id="RowNo' + x + '">' +
        '<td><span class="Priority_No"></span></td>'+
'<td class="Priority" data-label="Priority"><div class="drop"><select class="selectpicker options wid" id="Pri' + x + '" name="Pri' + (x - 1) + '" data-style="btn-success"><option value="L">Low</option><option value="M">Medium</option><option value="H">High</option></select></div><input type="hidden" id="Uni_Id' + x + '" name="Uni' + (x-1) + '" value="' +TaskId  + '"/></td>' +
        "<td class='Categories' data-label='Categories'><div class='dropdown'><select class='selectpicker widt' name='Categories" + (x - 1) + "' data-style='btn-success' id='dropdownMenuButton" + (x - 1) + "'><option hidden>Categories</option></select></div></td>" +
        '<td class="ConnetTo" data-label="Connet To"><div class="dropdown"><select class="selectpicker width" name="ConnetTo' + (x - 1) + '" data-style="btn-success" id="Client' + (x - 1) + '"><option hidden>Client</option></select></div></td>' +
        '<td class="Task" data-label="Task"><div class="form-group"><textarea class="form-control" rows="1" name="note' + (x - 1) + '" id="comment' + (x - 1) + '"></textarea></div></td>' +
        '<td class="Option1" data-label="Option 1"><div class="OptionDetails"><div class="dropdown"><select class="selectpicker options"  name="Option1' + (x - 1) + '" data-style="btn-success" id="Option1' + (x - 1) + '"><option hidden>Op1</option><option value="Meet">Meet</option><option value="Call">Call</option><option value="Text">Text</option></select></div><div class="form-check" options><label for="Op1' + (x - 1) + '"><input type="hidden" name="Op1' + (x - 1) + '" value="False" ><input class="form-check-input" name="Op1' + (x - 1) + '" type="checkbox" value="True" id="Op1' + (x - 1) + '"></label></div></div></td>' +
        '<td class="Option2" data-label="Option 2"><div class="OptionDetails"><div class="dropdown"><select class="selectpicker options" name="Option2' + (x - 1) + '" data-style="btn-success" id="Option2' + (x - 1) + '"><option hidden>Op2</option><option value="Call">Call</option><option value="Meet">Meet</option><option value="Text">Text</option></select></div><div class="form-check"><label for="Op2' + (x - 1) + '"><input type="hidden" name="Op2' + (x - 1) + '" value="False" ><input class="form-check-input" type="checkbox" name="Op2' + (x - 1) + '" value="True" id="Op2' + (x - 1) + '"></label></div></div></td>' +
        '<td class="Option3" data-label="Option 3"><div class="OptionDetails"><div class="dropdown4"><select class="selectpicker options" name="Option3' + (x - 1) + '" data-style="btn-success" id="Option3' + (x - 1) + '"><option hidden>Op3</option><option value="Text">Text</option><option value="Meet">Meet</option><option value="Call">Call</option></select></div><div class="form-check"><label for="Op3' + (x - 1) + '"><input type="hidden" name="Op3' + (x - 1) + '" value="False"><input class="form-check-input" type="checkbox" name="Op3' + (x - 1) + '" value="True"id="Op3' + (x - 1) + '"></label></div></div> </td>' +
        '<td class="Status" data-label="Status"><select class="selectpicker important ar" name="Status' + (x - 1) + '" data-style="btn-info" id="arrow' + (x - 1) + '" onchange="this.className=this.options[this.selectedIndex].className"><option class="important ar">Not Started</option><option class="InProgress ar">In Progress</option><option class="InReview ar">In Review</option><option class="Completed ar">Completed</option><option class="Cancelled ar">Cancelled</option></select></div></td>' +
        '<td class="Deletebtn" data-label="Action"><div class="ButtonWrapper1"><a href="includes/deletetask.php?id=' + y + '" id="DeleteBtn' + (x - 1) + '" class="btn btn-outline-secondary AddRemove Trash" c><i class="fas fa-trash-alt"></i></a></div><div class="hide"><input name="rows[]" value=' + x +'></div></td></tr>';
        row1.className="all work";
            row1.id="RowNo"+x;
        var selectCategories = document.getElementById("dropdownMenuButton" + (x - 1));
    var selectClient = document.getElementById("Client" + (x - 1));
    for (const val of value) {
        var option = document.createElement("option");
        option.text = val;

        selectCategories.appendChild(option);

    }
    // console.log(value1);
    for (const val of value1) {
        var option1 = document.createElement("option");
        option1.text = val;

        selectClient.appendChild(option1);

    }
    var table = document.getElementById("Table");
var y = table.rows.length;
var xy=document.getElementById("Uni_Id1").value;
console.log("xy",xy);


y=y-1;
for(this.i=0;this.i<y;this.i++){
    console.log("Option1"+this.i);
    var elem= document.getElementById("Option1"+this.i);
   elem.addEventListener("click", func, false);
   function func(e){
       var a=e.target.id;
       console.log(a);
       No=a.slice(7);
       console.log(No);
   }
       document.getElementById("Option1"+this.i).onchange=function(e){
var el=this;
if(el.value==="Meet"){
op="#Option2"+No;
console.log(op);
document.querySelectorAll(op+" option").forEach(option => option.remove())
var values = ["Op2", "Call", "Text"];
op1="Option2"+No;
var select = document.getElementById(op1);
for (const val of values) {
var option = document.createElement("option");
if (val == "Op2") {
option.setAttribute("hidden", true);
option.text = val;
}
option.text = val;
option.value = val;


select.appendChild(option);
}
}
else if (el.value === "Call") {
    op="#Option2"+No;
console.log(op);
document.querySelectorAll(op+" option").forEach(option => option.remove())
var values = ["Op2", "Meet", "Text"];
op1="Option2"+No;
console.log(op1);
var select = document.getElementById(op1);
for (const val of values) {
var option = document.createElement("option");
if (val == "Op2") {
option.setAttribute("hidden", true);
option.text = val;
}
option.text = val;  
option.value = val;


select.appendChild(option);
}    



}else if (el.value === "Text") {

op="#Option2"+No;
console.log(op);
document.querySelectorAll(op+" option").forEach(option => option.remove())
var values = ["Op2", "Meet", "Call"];
op1="Option2"+No;
var select = document.getElementById(op1);
console.log(select);
for (const val of values) {
var option = document.createElement("option");
if (val == "Op2") {
option.setAttribute("hidden", true);
option.text = val;
}
option.text = val;
option.value = val;


select.appendChild(option);
}    

}
}
var elem1= document.getElementById("Option2"+this.i);
   elem1.addEventListener("click", func1, false);
   function func1(e){
       var b=e.target.id;
       console.log(b);
       No1=b.slice(7);
       console.log(No1);
   }


document.getElementById("Option2"+this.i).onchange=function(e){
var e1=this;
var Op33="#Option3"+No1;
var op3="Option3"+No1;
console.log(op3);
var option1select="Option1"+No1;
var source=document.getElementById(option1select).value;
if(source === "Meet" && e1.value=== "Call"){
document.querySelectorAll(Op33+" option").forEach(option => option.remove())
var values = ["Text"];
var select = document.getElementById(op3);
console.log(select);
for (const val of values) {
var option = document.createElement("option");

option.text = val;
option.value = val;


select.appendChild(option);
}    
}
else if (source === "Meet" && e1.value === "Text") {
document.querySelectorAll(Op33+" option").forEach(option => option.remove())
var values = ["Call"];
var select = document.getElementById(op3);
console.log(select);
for (const val of values) {
var option = document.createElement("option");

option.text = val;
option.value = val;


select.appendChild(option);
}    


}
else if (source === "Text" && e1.value === "Meet") {
document.querySelectorAll(Op33+" option").forEach(option => option.remove())
var values = ["Call"];
var select = document.getElementById(op3);
console.log(select);
for (const val of values) {
var option = document.createElement("option");

option.text = val;
option.value = val;


select.appendChild(option);
}    

}
else if (source === "Text" && e1.value === "Call") {
document.querySelectorAll(Op33+" option").forEach(option => option.remove())
var values = ["Meet"];
var select = document.getElementById(op3);
console.log(select);
for (const val of values) {
var option = document.createElement("option");

option.text = val;
option.value = val;


select.appendChild(option);
}    

}
else if (source === "Call" && e1.value === "Meet") {
document.querySelectorAll(Op33+" option").forEach(option => option.remove())
var values = ["Text"];
var select = document.getElementById(op3);
console.log(select);
for (const val of values) {
var option = document.createElement("option");

option.text = val;
option.value = val;


select.appendChild(option);
}    

}
else if (source === "Call" && e1.value === "Text") {
document.querySelectorAll(Op33+" option").forEach(option => option.remove())
var values = ["Meet"];
var select = document.getElementById(op3);
console.log(select);
for (const val of values) {
var option = document.createElement("option");

option.text = val;
option.value = val;


select.appendChild(option);
}    

}

}

}
});
</script>

    <!-- <script>
      function sortTable() {
      var table, rows, switching, i, x, y, shouldSwitch;
      table = document.getElementById("Table");
      switching = true;
      /*Make a loop that will continue until
      no switching has been done:*/
      while (switching) {
        //start by saying: no switching is done:
        switching = false;
        rows = table.rows;
        /*Loop through all table rows (except the
        first, which contains table headers):*/
        for (i = 1; i < (rows.length - 1); i++) {
          //start by saying there should be no switching:
          shouldSwitch = false;
          /*Get the two elements you want to compare,
          one from current row and one from the next:*/
          x = rows[i].getElementsByTagName("span")[0];
          console.log(x);
          y = rows[i + 1].getElementsByTagName("span")[0];
          console.log(y);
          console.log(x.innerHTML);
          console.log(y.innerHTML);
          //check if the two rows should switch place:
          if (Number(x.innerHTML) > Number(y.innerHTML)) {
            //if so, mark as a switch and break the loop:
            shouldSwitch = true;
            break;
          }
        }
        if (shouldSwitch) {
          /*If a switch has been marked, make the switch
          and mark that a switch has been done:*/
          rows[i].parentNode.insertBefore(rows[i + 1], rows[i]);
          switching = true;
        }
      }
    }
    </script> -->
        <!-- <script src="https://code.jquery.com/ui/1.12.0/jquery-ui.min.js"></script> -->

    <!-- // <script src="https://code.jquery.com/jquery.js"></script> -->
<!--  -->
        
    <!-- <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.bundle.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script> -->

</body>
<!-- <link rel="stylesheet" type="text/css" href="css/jquery.datetimepicker.min.css" /> -->
<!-- <script src="js/jquery.datetimepicker.js"></script> -->


</html>
