

<?php
//Import PHPMailer classes into the global namespace
//These must be at the top of your script, not inside a function
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\SMTP;
use PHPMailer\PHPMailer\Exception;

require 'PHPMailer/src/Exception.php';  
require 'PHPMailer/src/PHPMailer.php';
require 'PHPMailer/src/SMTP.php';
require_once "includes/config.php";





//Load Composer's autoloader
// require 'vendor/autoload.php';

//Instantiation and passing `true` enables exceptions
$valid="";
$err="";
$mail = new PHPMailer(true);

if(isset($_POST["submit"])){

$emailTo=$_POST["email"];
// echo  $emailTo;
$sql = "SELECT `email` FROM users WHERE `email`='$emailTo'";
$select = mysqli_query($connection, $sql);
// if(!$select){
//     echo "ajay";
// }
if($select || mysqli_num_rows($select) == 0){
    // echo "ajay";
    $checkrows = mysqli_num_rows($select);
}
// $checkrows = mysqli_num_rows($select);

if ($checkrows > 0) {

    $code=uniqid(true);
$query=mysqli_query($connection,"INSERT INTO resetpassword(code,email) VALUES ('$code','$emailTo') ");
if(!$query)
{
    exit("error");
}


    try {
        //Server settings
        $mail->isSMTP();                                            //Send using SMTP
        $mail->Host       = 'smtp.gmail.com';                     //Set the SMTP server to send through
        $mail->SMTPAuth   = true;                                   //Enable SMTP authentication
        $mail->Username   = 'marchingsolider1010@gmail.com';                     //SMTP username
        $mail->Password   = 'confirmme';                               //SMTP password
        $mail->SMTPSecure = PHPMailer::ENCRYPTION_STARTTLS;         //Enable TLS encryption; `PHPMailer::ENCRYPTION_SMTPS` encouraged
        $mail->Port       = 587;                                    //TCP port to connect to, use 465 for `PHPMailer::ENCRYPTION_SMTPS` above
    
        $mail->SMTPOptions = array(
            'ssl' => array(
                'verify_peer' => false,
                'verify_peer_name' => false,
                'allow_self_signed' => true
            )
        );
    
        //Recipients
        $mail->setFrom('marchingsolider1010@gmail.com', 'TaskSchedular');
        $mail->addAddress("$emailTo");     //Add a recipient
        // $mail->addAddress('ellen@example.com');               //Name is optional
        // $mail->addReplyTo('info@example.com', 'Information');
    
        //Attachments
        // $mail->addAttachment('/var/tmp/file.tar.gz');         //Add attachments
        // $mail->addAttachment('/tmp/image.jpg', 'new.jpg');    //Optional name
    
        //Content
        $url="http://" . $_SERVER["HTTP_HOST"] . dirname($_SERVER["PHP_SELF"]) . "/resetpwd.php?code=$code";
        // echo $url;
        $mail->isHTML(true);                                  //Set email format to HTML
        $mail->Subject = 'Your Password Reset Link';
        $mail->Body    = "<h1>You requested a password reset</h1>
                             :<a href='$url'>Click Me!</a> to reset";
        $mail->AltBody = '';
        $valid="Mail Sent!Please Check Your Inbox..."; 
        $err="";
        $mail->send();
       

    // else{
    //      $valid="Error!";
    // }
             

        // echo 'reset password link has been sent to email';
    } catch (Exception $e) {
        echo "Message could not be sent. Mailer Error: {$mail->ErrorInfo}";
    }
    

}

else{
    $err="Email Not Found!";
  }   
}


?>
<!-- <form method="POST">
<input type="text " name="email" placeholder="Email" autocomplete="off">
<br>
<input type="submit" name="submit" value="send email">
</form> -->
<html lang="en">
    <head>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
        <link rel="stylesheet" type="text/css" href="css/forget-password.css">
        <title>Reset PASSWORD</title>
    </head>
    
    <body>
        <div class="d-flex align-items-center light-blue-gradient" style="height: 100vh;">
            <div class="container" >
                <div class="d-flex justify-content-center">
                    <div class="col-md-7">
                        <div class="card rounded-0 shadow">
                            <div class="card-body">
                                <h3>Forget Password</h3>
                                
                                <form method="POST" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>">
                                    <div class="form-group">
                                        <label for="exampleInputEmail1">Please enter your email address to reset password.</label>
                                        <input type="text" class="form-control" name="email" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter mail id">
                                        <small id="emailHelp" class="form-text text-muted">we'll send forget password link on your email.</small>
                                    </div>
                                    <button type="submit" name="submit"class="btn btn-primary form-control <?php echo (($valid)) ? 'is-valid' : ''; ?><?php echo (($err)) ? 'is-invalid' : ''; ?>">SEND MAIL </button>
                                    <span class="valid-feedback"><?php echo $valid; ?></span>
                                    <span class="Invalid-feedback"><?php echo $err; ?></span>
                                    
                                    
                                </form>
                                 
                            </div>
                        </div>
                    </div>
                </div>

                <!-- Optional JavaScript -->
                <!-- jQuery first, then Popper.js, then Bootstrap JS -->
                <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
                <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
                <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
            </div>
    </body>
</html>


