<?php
include 'includes/sessionstart.php';
include 'includes/UserDetails.php';
include 'includes/DashboardCount.php';
?>
<html lang="en">
<link href="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<!-- <script src="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script> -->
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Dashboard</title>
  
  <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@200;300;400;600&display=swap" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Material+Icons|Material+Icons+Outlined|Material+Icons+Two+Tone|Material+Icons+Round|Material+Icons+Sharp" rel="stylesheet">
  <link rel='stylesheet' href='https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css'>
        <link rel="stylesheet" href="css/navStyleProfile.css">
        <link href="css/Dashboardstyle.css" rel="stylesheet" />
        <link href="css/bootstrap.min.css" rel="stylesheet"/>
        <link href="https://cdn.jsdelivr.net/npm/simple-datatables@latest/dist/style.css" rel="stylesheet" />
        <link rel="stylesheet" href="css/UserAccStyle.css">
        <link rel="stylesheet" href="css/UserAccountNavstyle.css">
        <script src='https://kit.fontawesome.com/a076d05399.js'></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/js/all.min.js" crossorigin="anonymous"></script>
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <!-- //datepicker -->
    
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
  <!-- <link rel="stylesheet" href="/resources/demos/style.css"> -->
  <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
  <script src="js/dateDash.js"></script>
<!-- <style>
.breadcrumb-item+.breadcrumb-item::before {
    display:none;
}
</style> -->
  </head>
<body>
  


<!------ Include the above in your HEAD tag ---------->
<input type="checkbox" id="menu-toggle" />
  <label for="menu-toggle" class="menu-icon"><i class="fa fa-bars"></i></label>
  <div class="content-container">
    <div class="site-title">
      <h1>Dashboard</h1>
      <div class="action">
        <div class="profile" onclick="menuToggle();">
            <img src="img/user.png" alt="">
        </div>

        <div class="menu">
        
            <h3>
                @<?php echo $user; ?>
                <div>
              <?php echo $usertype; ?>
                </div>
                
            </h3>
            <ul>
                <li>
                    <span class="material-icons icons-size">person</span>
                    <a href="UserAccount.php">My Profile</a>
                </li>
                <!-- <li>
                    <span class="material-icons icons-size">mode</span>
                    <a href="#">Edit Account</a>
                </li> -->
                <li>
                <!-- <i class="fas fa-sign-out-alt"></i> -->
                    <span class="fas fa-sign-out-alt"></span>
                    <a href="includes/logout.php">Logout</a>
                </li>
          
            </ul>
        </div>
    </div>
    </div>
    <div class="content">
   
    <div id="layoutSidenav_content">
                <main>
                <form method="POST"  id="myForm">
                    <div class="container-fluid px-4">
                    <div  class="input-group date" id="vcal" data-date-format="dd-mm-yyyy">
                <input id="datepicker" class="aj" name="date" placeholder="Date" value="<?php echo $datepick; ?>">
<span class="input-group-addon" id="CalenderIcon"><i id="calicn" class="fas fa-calendar-week"></i></span>
</form>
                </div>
                        <!-- <h1 class="mt-4">Dashboard</h1> -->
                        <ol class="breadcrumb mb-4">
                            <li class="breadcrumb-item active">Total Tasks:<?php echo $CountTask; ?></li>
<!-- 
                            <li class="breadcrumb-item active"></li> -->
                        </ol>
                        <div class="row"style="margin-top: 45px;">
                        <div class="col-xl-3 col-md-6">
                                <div class="card bg-primary text-white mb-4"style=" background-color: rgb(117 121 130) !important; ">
                                    <div class="card-body">Today`s Tasks(Total)</div>
                                    <div class="card-footer d-flex align-items-center justify-content-between">
                                        <p class="small text-white stretched-link" ><?php echo $CountTodaysTask;?>Tasks</p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xl-3 col-md-6">
                                <div class="card bg-primary text-white mb-4" style=" background-color: rgb(252, 113, 80) !important; ">
                                    <div class="card-body">Not Started</div>
                                    <div class="card-footer d-flex align-items-center justify-content-between">
                                        <p class="small text-white stretched-link" ><?php echo $CountNs; ?>Tasks</p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xl-3 col-md-6">
                                <div class="card bg-primary text-white mb-4"style=" background-color: rgb(81, 133, 252) !important; ">
                                    <div class="card-body">In Progress</div>
                                    <div class="card-footer d-flex align-items-center justify-content-between">
                                        <p class="small text-white stretched-link" ><?php echo $CountIP; ?> Tasks</p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xl-3 col-md-6">
                                <div class="card bg-warning text-white mb-4"style=" background-color:  rgb(252, 174, 81) !important; ">
                                    <div class="card-body">In Review</div>
                                    <div class="card-footer d-flex align-items-center justify-content-between">
                                        <p class="small text-white stretched-link" ><?php echo $CountIR; ?> Tasks</p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xl-3 col-md-6">
                                <div class="card bg-success text-white mb-4"style=" background-color: rgb(77, 215, 120) !important; ">
                                    <div class="card-body">Completed</div>
                                    <div class="card-footer d-flex align-items-center justify-content-between">
                                        <p class="small text-white stretched-link" ><?php echo $CountCom; ?> Tasks</p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xl-3 col-md-6">
                                <div class="card bg-danger text-white mb-4"style=" background-color: rgb(255, 91, 91) !important; ">
                                    <div class="card-body">Canceled</div>
                                    <div class="card-footer d-flex align-items-center justify-content-between">
                                        <p class="small text-white stretched-link" ><?php echo $CountCan; ?> Tasks</p>
                                    </div>
                                </div>
                                
                            </div>
                        </div>
                        <!-- <div class="row">
                            <div class="col-xl-6">
                                <div class="card mb-4">
                                    <div class="card-header">
                                        <i class="fas fa-chart-area me-1"></i>
                                        inc
                                    </div>
                                    <div class="card-body"><canvas id="myAreaChart" width="100%" height="40"></canvas></div>
                                </div>
                            </div>
                            <div class="col-xl-6">
                                <div class="card mb-4">
                                    <div class="card-header">
                                        <i class="fas fa-chart-bar me-1"></i>
                                        inc
                                    </div>
                                    <div class="card-body"><canvas id="myBarChart" width="100%" height="40"></canvas></div>
                                </div>
                            </div>
                        </div> -->
                        <div class="card mb-4">
                            
                           
                        </div>
                    </div>
                </main>
                <footer class="py-4 bg-light mt-auto">
                    <div class="container-fluid px-4">
                        <div class="d-flex align-items-center justify-content-between small">
                            <!-- <div class="text-muted">Copyright &copy; Task Management 2021</div> -->
                            <!-- <div>
                                <a href="#">Privacy Policy</a>
                                &middot;
                                <a href="#">Terms &amp; Conditions</a>
                            </div> -->
                        </div>
                    </div>
                </footer>
            </div>
            
        </div>
        </div>
  </div>
  <div class="slideout-sidebar">
  <ul>
  <li <?php if ($usertype=='User'){ echo "class='hide'" ;}?> ><a href="Definition.php" style="color:unset;">Definition</a></li>
  <li><a href="Dashboard.php" style="color: #49a0e3;font-size: 22px;">Dashboard</a></li>
      <li><a href="TaskEntry.php" style="color:unset;">Entry</a></li>
      <li><a href="view1.php" style="color:unset;">View</a></li>
     
      <!-- <li><a href="UserAccount.php">UserAccount</a></li> -->
      <!-- <li><a href="logout.php">Logout</a></li> -->
      
      
    </ul>
  </div>
  <script src="js/MenuToggle.js"></script>

<script>
$(document).ready(function() {
const date = new Date();
var formattedDate = date.toLocaleDateString('en-GB', {
  day: 'numeric', month: 'long', year: 'numeric'
});

// $("#datepicker").val(formattedDate);
$("#datepicker").on("change", function() {
    
    $('form#myForm').submit();
});
});
</script>


        </body>
</html>

