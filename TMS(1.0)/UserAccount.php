<?php
include 'includes/sessionstart.php';
include 'includes/UserDetails.php';
?>
<html lang="en">
<link href="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<!-- <script src="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script> -->
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>User Account</title>
  
  <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@200;300;400;600&display=swap" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Material+Icons|Material+Icons+Outlined|Material+Icons+Two+Tone|Material+Icons+Round|Material+Icons+Sharp" rel="stylesheet">
  <link rel='stylesheet' href='https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css'>
        <link rel="stylesheet" href="css/navStyleProfile.css">
        <link rel="stylesheet" href="css/UserAccStyle.css">
        <link rel="stylesheet" href="css/UserAccountNavstyle.css">
        <script src='https://kit.fontawesome.com/a076d05399.js'></script>
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
  </head>
<body>
  


<!------ Include the above in your HEAD tag ---------->
<input type="checkbox" id="menu-toggle" />
  <label for="menu-toggle" class="menu-icon"><i class="fa fa-bars"></i></label>
  <div class="content-container">
    <div class="site-title">
      <h1>User Account</h1>
      <div class="action">
        <div class="profile" onclick="menuToggle();">
            <img src="img/user.png" alt="">
        </div>

        <div class="menu">
        
            <h3>
                @<?php echo $user; ?>
                <div>
              <?php echo $usertype; ?>
                </div>
                
            </h3>
            <ul>
                <li>
                    <span class="material-icons icons-size">person</span>
                    <a href="UserAccount.php">My Profile</a>
                </li>
                <!-- <li>
                    <span class="material-icons icons-size">mode</span>
                    <a href="#">Edit Account</a>
                </li> -->
                <li>
                <!-- <i class="fas fa-sign-out-alt"></i> -->
                    <span class="fas fa-sign-out-alt"></span>
                    <a href="includes/logout.php">Logout</a>
                </li>
          
            </ul>
        </div>
    </div>
    </div>
    <div class="content">
<div class="container emp-profile">
            <form method="post" id="post">
        
           
              <div class="aj">
                        <div class="tab-content profile-tab" id="myTabContent">
                            <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
                                        <div class="row">
                                            <div class="col-md-6 Con">
                                                <label class="bold">User Id:</label>
                                            </div>
                                            <div class="col-md-6 Con">
                                                <p id="User"><?php echo $user; ?></p>
                                                <!-- <input type="text" name="User" placeholder="Enter the Username"/> -->
                                               
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6 Con">
                                                <label class="bold">Full Name:</label>
                                            </div>
                                            <div class="col-md-6 Con">
                                                <p id="fullname"><?php echo $fullname; ?></p>
                                                <input type="text" name="Full" class="hide" id="InputName" placeholder="Enter the Fullname"/>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6 Con">
                                                <label class="bold">Email:</label>
                                            </div>
                                            <div class="col-md-6 Con">
                                                <p id="email"><?php echo $email ?></p>
                                                <input type="email" name="Email" id="InputEmail" class="hide" placeholder="Enter the Email"/>
                                                
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6 Con">
                                                <label class="bold">Phone:</label>
                                            </div>
                                            <div class="col-md-6 Con">
                                                <p id="phone"><?php echo $phone; ?></p>
                                                <input type="tel" name="Phone" class="hide" id="InputPhone" placeholder="Enter the PhoneNumber"/>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6 Con">
                                                <label class="bold">Created At:</label>
                                            </div>
                                            <div class="col-md-6 Con" style>
                                                <p><?php echo $created; ?></p>
                                                
                                                
                                                
                                            </div>
                                            <div class="col-md-6 Con" style>
                                            <div class="ButtonWrapper">
                <input type="button" name="Edit" class="btn btn-outline-secondary edit" class="AddRemove" value="Edit" onclick="Edit_User(event)"/>
            </div>
                                                
                                            </div>
                                           
                                            
                                        </div>
                            </div>
                           
                        </div>
                    </div>
                </div>
            </form>           
        </div>
        </div>
  </div>
  <div class="slideout-sidebar">
  <ul>
  <li <?php if ($usertype=='User'){ echo "class='hide'" ;}?> ><a href="Definition.php" style="color:unset;">Definition</a></li>
<li <?php if ($usertype=='Admin'){ echo "class='hide'" ;}?>><a href="Dashboard.php" style="color:unset;">Dashboard</a></li>
      <li><a href="TaskEntry.php" style="color:unset;">Entry</a></li>
      <li><a href="view1.php" style="color:unset;">View</a></li>
     
      <!-- <li><a href="UserAccount.php">UserAccount</a></li> -->
      <!-- <li><a href="logout.php">Logout</a></li> -->
      
      
    </ul>
  </div>
  <script src="js/MenuToggle.js"></script>
  <script>
      function Edit_User(e){
        if (e.target.classList.contains("edit")) {
            e.target.parentElement.parentElement.parentElement.parentElement.classList.toggle("editable");
            var ParentElement=e.target.parentElement.parentElement.parentElement.parentElement;
var fullname=ParentElement.querySelector("#fullname");
var email=ParentElement.querySelector("#email");
var phone=ParentElement.querySelector("#phone");
var InputName=ParentElement.querySelector("#InputName");
var InputEmail=ParentElement.querySelector("#InputEmail");
var InputPhone=ParentElement.querySelector("#InputPhone");
console.log(InputName);
console.log(fullname);

console.log(e.target.parentElement.parentElement.parentElement.parentElement)
      } 
      if (e.target.parentElement.parentElement.parentElement.parentElement.classList.contains("editable")) {
          InputName.value=fullname.textContent;
          InputEmail.value=email.textContent;
          InputPhone.value=phone.textContent;
          InputName.classList.remove("hide");
          InputEmail.classList.remove("hide");
          InputPhone.classList.remove("hide");
          fullname.classList.add("hide");
          email.classList.add("hide");
          phone.classList.add("hide");
          var a=e.target;
          a.value="Save";   
        //   a.type="submit";
          a.setAttribute("name","Submit1");
          var user=document.getElementById("User").innerHTML;
          console.log(user);
          document.cookie = "UserName="+user;
}
else{
    var a=e.target;
          a.value="Edit";
          InputName.classList.add("hide");
          InputEmail.classList.add("hide");
          InputPhone.classList.add("hide");
          fullname.classList.remove("hide");
          email.classList.remove("hide");
          phone.classList.remove("hide");
          fullname.textContent=InputName.value;
          email.textContent=InputEmail.value;
          phone.textContent=InputPhone.value;
         
          console.log(fullname);
          document.getElementById("post").submit();


}
    }
  </script>

        </body>
</html>
<?php


if ($_SERVER["REQUEST_METHOD"] == "POST") {
    require_once "includes/config.php";
    $User=$_COOKIE['UserName'];
    echo $User;
    $check = mysqli_query($connection, "select * from users where username='$User'");
    $checkrows = mysqli_num_rows($check);
    if ($checkrows > 0) {
        $Full=$_POST["Full"];
        $email=$_POST["Email"];
        $phone=$_POST["Phone"];
        echo $Full;
        $qu = "UPDATE `users` SET `fullname`='$Full',`email` = '$email', `phone` = '$phone' where `users`.`username`='$User'";
        $check1 = mysqli_query($connection, $qu);
        
    }
    mysqli_close($connection);
    echo("<meta http-equiv='refresh' content='1'>");
}
?>
