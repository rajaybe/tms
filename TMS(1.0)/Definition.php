<?php
include 'includes/sessionstart.php';
include 'includes/UserDetails.php';

?>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Definition</title>
 
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
    <link rel='stylesheet' href='https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css'>
    <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@200;300;400;600&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Material+Icons|Material+Icons+Outlined|Material+Icons+Two+Tone|Material+Icons+Round|Material+Icons+Sharp" rel="stylesheet">
    <link rel="stylesheet" href="css/UserAccountNavstyle.css"/>
    <link rel="stylesheet" href="css/StyleDefinition.css"/>
    <link href="//netdna.bootstrapcdn.com/twitter-bootstrap/2.3.2/css/bootstrap-combined.no-icons.min.css" rel="stylesheet">
<link href="//netdna.bootstrapcdn.com/font-awesome/3.2.1/css/font-awesome.css" rel="stylesheet">
<script src='https://kit.fontawesome.com/a076d05399.js'></script>
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
   
<link rel='stylesheet' href='https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css'>
      <link rel='stylesheet' href="css/navStyleDefintion.css">
        <!-- <link rel="stylesheet" href="style123.css"/> -->
</head>
<body>

<input type="checkbox" id="menu-toggle" />
  <label for="menu-toggle" class="menu-icon"><i class="fa fa-bars"></i></label>
  <div class="content-container">
    <div class="site-title">
      <h1>Definition</h1>
      <div class="action left">
        <div class="profile" onclick="menuToggle();">
            <img src="img/user.png" alt="">
        </div>

        <div class="menu">
        
            <h3>
                @<?php echo $user; ?>
                <div>
              <?php echo $usertype; ?>
                </div>
                
            </h3>
            <ul id="ul">
                <li>
                    <span class="material-icons icons-size">person</span>
                    <a href="UserAccount.php">My Profile</a>
                </li>
                <!-- <li>
                    <span class="material-icons icons-size">mode</span>
                    <a href="#">Edit Account</a>
                </li> -->
                <li>
                <!-- <i class="fas fa-sign-out-alt"></i> -->
                    <span class="fas fa-sign-out-alt"></span>
                    <a href="includes/logout.php">Logout</a>
                </li>
          
            </ul>
        </div>
    </div>
    </div>
    
    <div class="content">
  <form method="POST">
  <div class="form-group">
  <div style="margin-top: 78px;"><B>TYPE</B></div>


    <select class="form-select" aria-label="Default select example" id="selectField" name="Type">
  <option selected>Open this select menu</option>
  <option value="Categories">Categories</option>
  <option value="ConnectTo">Connect To</option>
  <!-- <option value="Status">Status</option> -->
</select>
  </div>
  <div class="form-group">
    <label for="exampleInputPassword1"><B>VALUE</B></label>
    <input type="TEXT" class="form-control" placeholder="Enter A New Value..." id="exampleInputPassword1" name="Value" style="width: 219px;height:45px;border: 2px solid black">
  </div>
  
  <input type="submit" id="Update" name="save" class="btn btn-primary" value="Submit"/>
  </div>
  <?php
  require_once "includes/config.php";
$query="SELECT * FROM `definition` WHERE 1";
$res= mysqli_query($connection, $query);
echo '<table class="table"  id="Table" width="100%">';
echo '<thead><th>Sno</th>
<th class="width">Name</th>
<th>Action</th></thead>';

while ($data = mysqli_fetch_array($res)) {
 
  echo'<tr change="'.$data['Type'].'">';
  echo'<td data-label="Sno"><span class="Priority_No"></span></td>';
  echo'<td data-label="Name"><span class="Id hide">'.$data['Id'].'</span><span class="Type hide">'.$data['Type'].'</span><span class="value">'.$data['Value'].'</span></td>';
  echo '<td data-label="Action" style="display: flex;"><div class="EditIcon"><i id="EditIcon" class="edit fas fa-pencil-alt" aria-hidden="true" onclick="edit(event)"></i></div><div class="DeleteIcon"><a href="includes/deletedefinition.php?id=' .$data['Id'] . '" id="DeleteBtn' .($data['Id']-1).  '" class="btn btn-outline-secondary AddRemove Trash" onClick=\'javascript: return confirm("Due You Want To Delete Task?");\'><i class="fas fa-trash-alt"></i></a></div></td>';
  echo'</tr>';
  
}
echo '</table>';

  ?>
</form>

  </div>
  <div class="slideout-sidebar">
  <ul>

      <li><a href="Definition.php" style="color: #49a0e3; font-size: 22px;" >Definition</a></li>
      <!-- <li ><a href="Dashboard.php" style="color:unset;">Dashboard</a></li> -->
      <li><a href="TaskEntry.php" style="color:unset;">Entry</a></li>
      <li><a href="view1.php" style="color:unset;">View</a></li>
      <!-- <li><a href="UserAccount.php">UserAccount</a></li> -->
      <!-- <li><a href="logout.php">Logout</a></li> -->
      
    </ul>
  </div>
 
  <?php
 
  
  if(isset($_POST['save'])){
    // if (isset($_POST['Type']) || isset($_POST['Value']) ) {
  $Type=$_POST['Type'];
 
  $Value=$_POST['Value'];
  
    // }
  if($Type!=""&& $Value!="" ){
    echo $Value;
    echo $Type;


  
  $sql="INSERT INTO `definition` (Type,Value) VALUES ('".$_POST["Type"]."','".$_POST["Value"]."')";
  if ($connection === false) {
    die("ERROR: Could not connect. " . mysqli_connect_error());
}
  $result = mysqli_query($connection, $sql);
    

  if($result)
  {
     echo "data stored";

  }
  else{
	  echo"insert data failed";
  }
}
mysqli_close($connection);
echo("<meta http-equiv='refresh' content='1'>");
  }
    
  if(isset($_POST['Update'])){
    // if (isset($_POST['Type']) || isset($_POST['Value']) ) {
  $Type=$_POST['Type'];
 $Value=$_POST['Value'];
  $Def_Id=$_COOKIE['DefinitionId'];
  echo $Value;
    echo $Type;


  $sql1="UPDATE `definition` SET `Type`='$Type',`Value`='$Value' WHERE `Id`=$Def_Id";

  if ($connection === false) {
      die("ERROR: Could not connect. " . mysqli_connect_error());
  }

  $result1 = mysqli_query($connection, $sql1);
    

  if($result1)
  {
     echo "data updated stored";

  }
  else{
	  echo"updated data failed";
  }
  mysqli_close($connection);
echo("<meta http-equiv='refresh' content='1'>");
}


  ?>
  <script src="js/MenuToggle.js"></script>
  <script>
        $(document).ready(function() {

function addRemoveClass(theRows) {

    theRows.removeClass("odd even");
    theRows.filter(":odd").addClass("odd");
    theRows.filter(":even").addClass("even");
}

var rows = $("table#Table tbody tr");
console.log(rows);

addRemoveClass(rows);


$("#selectField").on("change", function() {

    var selected = this.value;

    if (selected != "All") {

        rows.filter("[change=" + selected + "]").show();
        rows.not("[change=" + selected + "]").hide();
        var visibleRows = rows.filter("[change=" + selected + "]");
        addRemoveClass(visibleRows);
    } else {

        rows.show();
        addRemoveClass(rows);

    }

});
});
    </script>
    <script>
      function edit(e){
        var ParentElement=e.target.parentElement.parentElement.parentElement;
        var Categories=ParentElement.querySelector(".Type");
        var Value=ParentElement.querySelector(".value");
        var ID=ParentElement.querySelector(".Id").textContent;
        var TypeDropDown=document.getElementById("selectField");
        console.log(TypeDropDown);
        var ValueInput=document.getElementById("exampleInputPassword1");
        TypeDropDown.value=Categories.textContent;
        ValueInput.value=Value.textContent;
        var SubmitInput=document.querySelector("#Update");
        // SubmitInput.removeAttribute("name");
        SubmitInput.name="Update";
        SubmitInput.value="Update";
        document.cookie = "DefinitionId="+ID;


      }
    </script>
   
      
  

</body>


</html> 
