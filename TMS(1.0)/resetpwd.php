<?php
include("includes/config.php");
if(isset($_GET["code"])){
    $code=$_GET["code"];
setcookie("codeId", $code);
}
// $code=$_GET["code"];
// setcookie("myCookie", $value);

// echo $code;

$password = $confirm_password = $valid="";
$password_err = $confirm_password_err = "";

if($_SERVER["REQUEST_METHOD"] == "POST"){
    $code=$_COOKIE["codeId"];
    $getEmailquery= mysqli_query($connection,"SELECT email FROM resetPassword Where code='$code'");

    if(mysqli_num_rows($getEmailquery)==0){
        exit("error!Not Allowed[Kindly Give Forget Password Again!]");
    
    }

    if (empty(trim($_POST["password"]))) {
        $password_err = "Please enter a password.";
    } elseif (strlen(trim($_POST["password"])) < 6) {
        $password_err = "Password must have atleast 6 characters.";
    } else {
        $password = trim($_POST["password"]);
    }
    
    // Validate confirm password
    if (empty(trim($_POST["confirm_password"]))) {
        $confirm_password_err = "Please confirm password.";
    } else {
        $confirm_password = trim($_POST["confirm_password"]);
        if (empty($password_err) && ($password != $confirm_password)) {
            $confirm_password_err = "Password did not match.";
        }
    }
    if (empty($password_err) && empty($confirm_password_err)) {
        $password=password_hash($password, PASSWORD_DEFAULT); 
        $password = substr( $password, 0, 60 );
        $valid="Password Updated!";
        $row=mysqli_fetch_array($getEmailquery);
        $email=$row["email"];
        $query=mysqli_query($connection, "UPDATE `users` SET `password`='$password' WHERE `email`='$email'");
        if ($query) {
            $query=mysqli_query($connection, "DELETE FROM resetPassword WHERE code='$code'");
            // header("location: login.php");
        } else {
            exit("something x");
        }
    }
    // else{
    //     $valid="Error!";
    // }
}
?>
<!-- <form method="POST">
<input type="password" name="password" placeholder="new one" >
<br>
<input type="submit" name="submit" value="update password">
</form> -->
<!doctype html>
<html lang="en">
    <head>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
        <link rel="stylesheet" type="text/css" href="css/forget-password.css">
        <title>Forget password</title>
    </head>
    <body>
        <div class="d-flex align-items-center light-blue-gradient" style="height: 100vh;">
            <div class="container" >
                <div class="d-flex justify-content-center">
                    <div class="col-md-7">
                        <div class="card rounded-0 shadow">
                            <div class="card-body">
                                <h3>Forget Password</h3>
                                <form method="POST" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>">
                                    <div class="form-group">
                                        <label for="exampleInputEmail1">RESET PASSWORD</label>
                                        <input type="password" class="form-control <?php echo (!empty($password_err)) ? 'is-invalid' : ''; ?>" name="password" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter new password" value="<?php echo $password; ?>" >
                                        <span class="invalid-feedback"><?php echo $password_err; ?></span>
                                        <!-- <small id="emailHelp" class="form-text text-muted">we'll send forget password link on your email.</small> -->
                                    </div>
                                    <div class="form-group">
                                        <!-- <label for="exampleInputEmail1">RESET PASSWORD</label> -->
                                        <input type="password" class="form-control <?php echo (!empty($confirm_password_err)) ? 'is-invalid' : ''; ?>" name="confirm_password" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Retype the password without any hesitation" value="<?php echo $confirm_password; ?>">
                                        <span class="invalid-feedback"><?php echo $confirm_password_err; ?></span>
                                        <!-- <small id="emailHelp" class="form-text text-muted">we'll send forget password link on your email.</small> -->
                                    </div>
                                    <button type="submit" name="submit"class="btn btn-primary form-control <?php echo (($valid)) ? 'is-valid' : ''; ?>" >UPDATE </button>
                                    <span class="valid-feedback"><?php echo $valid; echo'<p style="text-align: center;
    margin-top: 25px;color: black;    font-size: 17px;
    ">Please <a href="login.php">Login here!</a>.</p>'; ?></span>
    <span class="invalid-feedback"><?php echo $password_err; ?></span>
                                    

                                </form>
                                 
                            </div>
                        </div>
                    </div>
                </div>

                <!-- Optional JavaScript -->
                <!-- jQuery first, then Popper.js, then Bootstrap JS -->
                <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
                <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
                <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
            </div>
    </body>
</html>
