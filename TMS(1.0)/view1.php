<?php
include 'includes/sessionstart.php';
include 'includes/UserDetails.php';
include 'includes/OptionValue.php';

?>
<html>
<head>
  <title>view page</title>
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="css/styleView.css"/>
  <script src='https://kit.fontawesome.com/a076d05399.js'></script>
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    
    <!-- <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet"> -->
  <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-eOJMYsd53ii+scO/bJGFsiCZc+5NDVN2yr8+0RDqr0Ql0h+rP48ckxlpbzKgwra6" crossorigin="anonymous">
        <!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script> -->
        <link rel='stylesheet' href='https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css'>
        <link rel="stylesheet" href="css/navStyleView1.css">
        <link href="https://code.jquery.com/ui/1.10.2/themes/smoothness/jquery-ui.css" rel="stylesheet"/>
        <!-- <script src="js/jquery.min.js"></script>         -->
        <!-- <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script> -->
    <!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/js/bootstrap-datepicker.js"></script> -->
       
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/js/bootstrap-datepicker.js"></script>
    <link rel='stylesheet' href='https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css'>
    <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@200;300;400;600&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Material+Icons|Material+Icons+Outlined|Material+Icons+Two+Tone|Material+Icons+Round|Material+Icons+Sharp" rel="stylesheet">
    <link rel="stylesheet" href="css/UserAccountNavstyle.css"/>
    
    <script src="js/jquery.min.js"></script>        
        <script src="js/date1.js"></script>

        <!-- For Filter Userid -->
        <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
  <!-- <link rel="stylesheet" href="/resources/demos/style.css"> -->
  <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
      
  <!-- <script src="scriptindi.js"></script> -->

</head>

<input type="checkbox" id="menu-toggle" />
  <label for="menu-toggle" class="menu-icon"><i class="fa fa-bars"></i></label>
  <div class="content-container">
    <div class="site-title">
      <h1>Task View</h1>
      <div class="action">
        <div class="profile" onclick="menuToggle();">
            <img src="img/user.png" alt="">
        </div>

        <div class="menu">
        
            <h3>
                @<?php echo $user; ?>
                <div>
              <?php echo $usertype; ?>
                </div>
               
            </h3>
            <ul id="ul">
                <li>
                    <span class="material-icons icons-size">person</span>
                    <a href="UserAccount.php">My Profile</a>
                </li>
                <!-- <li>
                    <span class="material-icons icons-size">mode</span>
                    <a href="#">Edit Account</a>
                </li> -->
                <li>
                <!-- <i class="fas fa-sign-out-alt"></i> -->
                    <span class="fas fa-sign-out-alt"></span>
                    <a href="includes/logout.php">Logout</a>
                </li>
                <!-- <li>
                    <span class="material-icons icons-size">monetization_on</span>
                    <a href="#">Budget</a>
                </li>
                <li>
                    <span class="material-icons icons-size">account_balance</span>
                    <a href="#">Finance</a>
                </li>
                <li>
                    <span class="material-icons icons-size">account_balance_wallet</span>
                    <a href="#">Wallet</a>
                </li> -->
            </ul>
        </div>
    </div>
    </div>
    <div class="content">
      <div class="Parent">
  <div class="date-wrapper" id="myDIV">
        <input type="button" onclick="AllFunction()" value="All" id="all" class="Navi">
        <input type="button" onclick="WorkFunction()" value="Work" id="work" class="Navi">
        <input type="button" onclick="PersonalFunction()" value="Personal" id="personal" class="Navi">
        <input type="button" onclick="ProspectFunction()" value="Prospect" id="prospect" class="Navi">
        </div>
        <div class="optionsDiv">
        Filter By Status:
        <select id="selectField">
            <option value="All" selected>All</option>
            <option value="InReview">In Review</option>
            <option value="Completed">Completed</option>
            <option value="Cancelled">Cancelled</option>
            <option value="NotStarted">Not Started</option>
            <option value="InProgress">In Progress</option>

        </select>
   
    </div>
 <script>
  $( function() {
    var availableTags = <?php echo json_encode($data_collector2); ?>;
    console.log(availableTags);
    availableTags.push("All");
    // var availableTags = [
    //   "ActionScript",
    //   "AppleScript",
    //   "Asp",
    //   "BASIC",
    //   "C",
    //   "C++",
    //   "Clojure",
    //   "COBOL",
    //   "ColdFusion",
    //   "Erlang",
    //   "Fortran",
    //   "Groovy",
    //   "Haskell",
    //   "Java",
    //   "JavaScript",
    //   "Lisp",
    //   "Perl",
    //   "PHP",
    //   "Python",
    //   "Ruby",
    //   "Scala",
    //   "Scheme"
    // ];
    $( "#tags" ).val("All");
    $( "#tags" ).autocomplete({
      source: availableTags
    });
  } );
  </script>
  <div class="ui-widget <?php if ($usertype=='User'){ echo "hide" ;}?> ">
  <label for="tags">Filter By UserId: </label>
  <input id="tags">
</div>


    
    </div>
    <?php

require_once "includes/config.php";
// echo 'Database Connected successfully';// we are now connected to database
$UserId=$user;
$UserType=$usertype;
// echo $UserId;
// echo $UserType;


 if ($UserType=='Admin') {
     $query="SELECT Id,Categories,Priority,Date,UserId,ConnectTo,Note,Option1,Option2,Option3,Op1Status,Op2Status,Op3Status,Status,Modified FROM `task` ORDER BY `UserId`";
     $result = mysqli_query($connection, $query);
 }
 else{
  $query="SELECT Id,Categories,Priority,Date,UserId,ConnectTo,Note,Option1,Option2,Option3,Op1Status,Op2Status,Op3Status,Status,Modified FROM `task` WHERE `UserId`='$UserId'";
  $result = mysqli_query($connection, $query);
 }

echo '<form method="POST" id="post"  name="POST">';
echo'<button onclick="window.print()" title="PrintScreen" class="print"><i class="fas fa-print"></i></button>';
echo'<div class="rowR" id="tt"><p>Total Tasks:</p><p id="numOfVisibleRows" >:</p></div>';
echo '<table class="table"  id="Table" width="100%">';
echo'<thead><th class="table1" onclick="sortTable()"  style="width:1.0% !important" title="Click To Sort" >Priority</th>
 <th style="width:10% !important" onclick="sortnumber()" title="Click To Sort">Asn.Date</th>
<th style="width:2.5% !important"   >ConnetTo</th>  
<th style="width: 68% !important;">Note</th>
<th style="width:2.5% !important">Option1</th>
<th style="width:2.5% !important">Option2</th>
<th style="width:2.5% !important">Option3</th>
<th style="width:10% !important">Status</th>
<th style="width: 10% !important;">Upd.Date</th>
<th style="width:1% !important">Action</th></thead>';
 //table headers

while ($data = mysqli_fetch_array($result)) {
    // we are running a while loop to print all the rows in a table
    $str = $data['Status'];
    $UserRef=$data['UserId'];
    if ($UserType=='User') 
    {$new_Ref="";
    }
    else{
        $new_Ref = str_replace(' ', '', $UserRef);
    }
$new_str = str_replace(' ', '', $str);
$dateCre=$data['Date'];
$pieces = explode(" ", $dateCre);
$Date_number=$pieces[0];
$timestamp = strtotime($dateCre);
$Month_number=date("n", $timestamp);
$Month_Text=date("M", $timestamp);
$Year_number=date("y", $timestamp);
$formatedDate=$Date_number .$Month_Text .$Year_number;
$dateMod=$data['Modified'];
$pie=explode(" ", $dateMod);
$Date_No=$pie[0];
$timest= strtotime($dateCre);
$month_no=date("n", $timest);
$Month_Txt=date("M", $timest);
$Year_no=date("y", $timest);
$forDate=$Date_No .$Month_Txt .$Year_no;








    echo'<tr class="All '.$data["Categories"].'" change="'.$new_str.'" user="'.$new_Ref.'"  title="'.$new_Ref.'">'; // printing table row
    echo '<td data-label="Priority" ><span>'.$data['Priority'].'</span><span class="Id hide">'.$data['Id'].'</span></td><td data-label="Date"><span class="DateTxt">'.$formatedDate.'</span><span class="hide">'.$Date_number.'</span><span class="hide">'.$Month_number.'</span><span class="hide">'.$Year_number.'</span>
    </td><td data-label="ConnectTo"><span class="ConnectTo" name="ConnectTo">'.$data['ConnectTo'].'</span>      <div class="dropdown">
<select class="selectpicker hide" id="Client" data-style="btn-success">

    <option value="Jai">Jai</option>
    <option value="Kamal">Kamal</option>
    <option value="Ajay">Ajay</option>
    <option value="Gopi">Gopi</option>
</select>
</div></td><td id="dspbtn" data-label="Note"><span class="Note" name="Note">'.$data['Note'].'</span> <div class="form-group" id="Txt">

<textarea class="form-control hide" rows="1" id="comment"></textarea>
</div></td><td data-label="Option1"><span class='.$data["Op1Status"].'>'.$data['Option1'].'</span></td><td data-label="Option2"><span class='.$data["Op2Status"].'>'.$data['Option2'].'</span></td><td data-label="Option3"><span class='.$data["Op3Status"].'>'.$data['Option3'].'</span></td><td data-label="Status"><span class="'.$new_str.' Status" name="Status">'.$data['Status'].'</span><select class="selectpicker important hide" data-style="btn-info" id="arrow">
<option>Not Started</option>
<option>In Progress</option>
<option>In Review</option>
<option>Completed</option>
<option>Cancelled</option>
</select></td>
<td><span class="" name="">'.$forDate.'</span> </td>
<td data-label="Action"><div class="remove"><i id="EditIcon" class="edit fas fa-pencil-alt" aria-hidden="true" onclick="edit(event)"></i></div></td>';
    echo'</tr>';
}

echo '</table>';
echo'</form>';





?>

    </div>
  </div>
  <div class="slideout-sidebar">
  <ul>
  <li <?php if ($UserType=='User'){ echo "class='hide'" ;}?> ><a href="Definition.php" style="color:unset;">Definition</a></li>
  <li <?php if ($usertype=='Admin'){ echo "class='hide'" ;}?>><a href="Dashboard.php" style="color:unset;">Dashboard</a></li>
      <li><a href="TaskEntry.php" style="color:unset;">Entry</a></li>
      <li><a href="view1.php" style="color: #49a0e3;font-size: 22px;">View</a></li>
      <!-- <li><a href="UserAccount.php">UserAccount</a></li> -->
      <!-- <li><a href="logout.php">Logout</a></li> -->
      
      
    </ul>
  </div>
 
  <script src="js/MenuToggle.js"></script>
  
  
  <script>
// Add active class to the current button (highlight it)
var header = document.getElementById("myDIV");
var btns = header.getElementsByClassName("Navi");
for (var i = 0; i < btns.length; i++) {
  btns[i].addEventListener("click", function() {
  var current = document.getElementsByClassName("active");
  if (current.length > 0) { 
    current[0].className = current[0].className.replace(" active", "");
  }
  this.className += " active";
  });
}
Rowcount();
function Rowcount(){
var numOfVisibleRows = $('tbody tr ').filter(function() {
  return $(this).css('display') !== 'none';
  }).length;
  console.log("gopi12",numOfVisibleRows);
  document.getElementById("numOfVisibleRows").innerHTML = numOfVisibleRows;
}
</script>
    
   
    <script>

        function myFunction() {
          var input, filter, table, tr, td, i, txtValue;
          input = document.getElementById("myInput");
          
          filter = input.value.toUpperCase();
          console.log(filter);
          table = document.getElementById("Table");
          tr = table.getElementsByTagName("tr");
          for (i = 0; i < tr.length; i++) {
            td = tr[i].getElementsByTagName("td")[1];
            if (td) {
              txtValue = td.textContent || td.innerText;
              if (txtValue.toUpperCase().indexOf(filter) > -1) {
                tr[i].style.display = "";
              } else {
                tr[i].style.display = "none";
              }
            }       
          }
        }
        </script>
          <script>
        $(document).ready(function() {

function addRemoveClass(theRows) {

    theRows.removeClass("odd even");
    theRows.filter(":odd").addClass("odd");
    theRows.filter(":even").addClass("even");
}

var rows = $("table#Table tbody tr");
console.log(rows);


addRemoveClass(rows);


$("#selectField").on("change", function() {

    var selected = this.value;

    if (selected != "All") {

        rows.filter("[change=" + selected + "]").show();
        rows.not("[change=" + selected + "]").hide();
        var visibleRows = rows.filter("[change=" + selected + "]");
        addRemoveClass(visibleRows);
    } else {

        rows.show();
        addRemoveClass(rows);

    }

});
$("#tags").on("change", function() {

var selectedVal = this.value;

if (selectedVal != "All") {

    rows.filter("[user=" + selectedVal + "]").show();
    rows.not("[user=" + selectedVal + "]").hide();
    var visibleRows = rows.filter("[user=" + selectedVal + "]");
    addRemoveClass(visibleRows);
} else {

    rows.show();
    addRemoveClass(rows);

}

});


});
    </script>
   


   <script>
    function sortTable() {
      var table, rows, switching, i, x, y, shouldSwitch;
  table = document.getElementById("Table");
  switching = true;
  /*Make a loop that will continue until
  no switching has been done:*/
  while (switching) {
    //start by saying: no switching is done:
    switching = false;
    rows = table.rows;
    /*Loop through all table rows (except the
    first, which contains table headers):*/
    for (i = 1; i < (rows.length - 1); i++) {
      //start by saying there should be no switching:
      shouldSwitch = false;
      /*Get the two elements you want to compare,
      one from current row and one from the next:*/
      x = rows[i].getElementsByTagName("span")[0];
      y = rows[i + 1].getElementsByTagName("span")[0];
      // console.log(x);
      // console.log(y);
      //check if the two rows should switch place:
      if (x.innerHTML.toLowerCase() > y.innerHTML.toLowerCase()) {
        //if so, mark as a switch and break the loop:
        shouldSwitch = true;
        break;
      }
    }
    if (shouldSwitch) {
      /*If a switch has been marked, make the switch
      and mark that a switch has been done:*/
      rows[i].parentNode.insertBefore(rows[i + 1], rows[i]);
      switching = true;
    }
  }
    }

    
    </script>
     <script>
    function sortnumber() {
      var table, rows, switching, i, x, y, shouldSwitch;
      table = document.getElementById("Table");
      switching = true;
      /*Make a loop that will continue until
      no switching has been done:*/
      while (switching) {
        //start by saying: no switching is done:
        switching = false;
        rows = table.rows;
        /*Loop through all table rows (except the
        first, which contains table headers):*/
        for (i = 1; i < (rows.length - 1); i++) {
          //start by saying there should be no switching:
          shouldSwitch = false;
          /*Get the two elements you want to compare,
          one from current row and one from the next:*/
          x = rows[i].getElementsByTagName("span")[3];
          console.log(x);
          y = rows[i + 1].getElementsByTagName("span")[3];
          console.log(y);
          console.log(x.innerHTML);
          console.log(y.innerHTML);
          //check if the two rows should switch place:
          if (Number(x.innerHTML) > Number(y.innerHTML)) {
            //if so, mark as a switch and break the loop:
            shouldSwitch = true;
            break;
          }
        }
        if (shouldSwitch) {
          /*If a switch has been marked, make the switch
          and mark that a switch has been done:*/
          rows[i].parentNode.insertBefore(rows[i + 1], rows[i]);
          switching = true;
        }
       
      }
      sortdate();
    }
function sortdate() {
      var table, rows, switching, i, x, y, shouldSwitch;
      table = document.getElementById("Table");
      switching = true;
      /*Make a loop that will continue until
      no switching has been done:*/
      while (switching) {
        //start by saying: no switching is done:
        switching = false;
        rows = table.rows;
        /*Loop through all table rows (except the
        first, which contains table headers):*/
        for (i = 1; i < (rows.length - 1); i++) {
          //start by saying there should be no switching:
          shouldSwitch = false;
          /*Get the two elements you want to compare,
          one from current row and one from the next:*/
          x = rows[i].getElementsByTagName("span")[4];
          console.log(x);
          y = rows[i + 1].getElementsByTagName("span")[4];
          console.log(y);
          console.log(x.innerHTML);
          console.log(y.innerHTML);
          //check if the two rows should switch place:
          if (Number(x.innerHTML) > Number(y.innerHTML)) {
            //if so, mark as a switch and break the loop:
            shouldSwitch = true;
            break;
          }
        }
        if (shouldSwitch) {
          /*If a switch has been marked, make the switch
          and mark that a switch has been done:*/
          rows[i].parentNode.insertBefore(rows[i + 1], rows[i]);
          switching = true;
        }
      }
    }
    
    </script>
    <script src="https://code.jquery.com/jquery.js"></script>
    <script>
      function edit(e){
        if (e.target.classList.contains("edit")) {
          console.log(e.target.parentElement.parentElement.parentElement);
          e.target.parentElement.parentElement.parentElement.classList.toggle("editable");
          var ParentElement=e.target.parentElement.parentElement.parentElement;
          var editedClientValue = ParentElement.querySelector(".selectpicker");
           var editedNoteValue = ParentElement.querySelector(".form-control");
          var editedStatusValue =ParentElement.querySelector(".important");
          // var editedDateValue=ParentElement.querySelector(".gop");
          var ClientTxt=ParentElement.querySelector(".ConnectTo");
        var NoteTxt=ParentElement.querySelector(".Note");
        var StatusValue=ParentElement.querySelector(".Status");
        // var DateTxt=ParentElement.querySelector(".DateTxt");
        var Id=ParentElement.querySelector(".Id").textContent;
        console.log(editedClientValue); 
     
       
          
          console.log(editedStatusValue);
      }
      if (e.target.parentElement.parentElement.parentElement.classList.contains("editable")){
        editedClientValue.value=ClientTxt.textContent;
        editedStatusValue.value=StatusValue.textContent;
        editedNoteValue.value=NoteTxt.textContent;
        // editedDateValue.value=DateTxt.textContent;
        // editedDateValue.classList.add("gopi");
        // editedDateValue.classList.remove("hide");
        editedClientValue.classList.remove("hide");
        editedNoteValue.classList.remove("hide");
  editedStatusValue.classList.remove("hide");
        ClientTxt.classList.add("hide");
        // DateTxt.classList.add("hide");
        NoteTxt.classList.add("hide");
        StatusValue.classList.add("hide");
        e.target.classList.remove("fa-pencil-alt");
        e.target.classList.add("fa-check");
        var a=e.target;
  a.setAttribute("name","Submit1");
}else {
  e.target.classList.add("fa-pencil-alt");
  e.target.classList.remove("fa-check");
 ClientTxt.classList.remove("hide");
//  DateTxt.classList.remove("hide");
  NoteTxt.classList.remove("hide");
  // editedDateValue.classList.remove("gopi");
  StatusValue.classList.remove("hide");
  editedClientValue.classList.add("hide");
  // editedDateValue.classList.add("hide");
  editedNoteValue.classList.add("hide");
  editedStatusValue.classList.add("hide");
  // editedDateValue.classList.remove("Dt"); 
  // DateTxt.textContent=editedDateValue.value;
  ClientTxt.textContent =editedClientValue.value;
  NoteTxt.textContent =editedNoteValue.value;
  StatusValue.textContent =editedStatusValue.value;
  var EditClient=editedClientValue.value;
  var EditNote=editedNoteValue.value;
  var EditStatus=editedStatusValue.value;
  // var EditDate=editedDateValue.value;
  console.log("Id",Id);
  document.cookie = "DeleteId="+Id;
  document.cookie = "Client="+EditClient;
  document.cookie = "Note="+EditNote;
  document.cookie = "Status="+EditStatus;
  // document.cookie ="Date="+EditDate;
  document.getElementById("post").submit();
  }
    }
    </script>
    
    
    <script src="js/test.js"></script>
    <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
    <script src="https://code.jquery.com/ui/1.12.0/jquery-ui.min.js"></script>
</body>
</html>


<?php
if ($_SERVER["REQUEST_METHOD"] == "POST") {
  require_once "includes/config.php";
    $Uni=$_COOKIE['DeleteId'];
    echo $Uni;
    $check = mysqli_query($connection, "select * from task where Id='$Uni'");
    $checkrows = mysqli_num_rows($check);
    if ($checkrows > 0) {
        $ConnectTO=$_COOKIE['Client'];
        $Note=$_COOKIE['Note'];
        $Status=$_COOKIE['Status'];
        $currentDateTime = date('Y-m-d H:i:s');
        $timestamp = strtotime($currentDateTime);
        $Date_number=date("j", $timestamp);
    $Month_number=date("n", $timestamp);
    $Month_Text=date("F", $timestamp);
    $Year_number=date("Y", $timestamp);
    $formatedDate=$Date_number.' '.$Month_Text.' '.$Year_number;
    echo $formatedDate;


        
        if ((($ConnectTO != "ClientProspect") && ($Note!= ""))) {
            $qu = "UPDATE `task` SET  `Modified`='$formatedDate',`ConnectTo` = '$ConnectTO', `Note` = '$Note', `Status` = '$Status' where `task`.`Id`='$Uni'";
        }
        $check1 = mysqli_query($connection, $qu);
    }
    
    mysqli_close($connection);

    echo("<meta http-equiv='refresh' content='1'>");
}

?>
